if(typeof gameInstance !== 'undefined' && gameInstance) {

var decoder = new TextDecoder("UTF-8"),
    encoder = new TextEncoder("UTF-8");

var log = console.log;


(function(console){
  console.save = function(data, filename){
    if(!data) {
        console.error('Console.save: No data')
        return;
    }
    if(!filename) filename = 'console.json'
    if(typeof data === "object"){
        data = JSON.stringify(data, undefined, 4)
    }
    var blob = new Blob([data], {type: 'text/json'}),
        e    = document.createEvent('MouseEvents'),
        a    = document.createElement('a')
    a.download = filename
    a.href = window.URL.createObjectURL(blob)
    a.dataset.downloadurl =  ['text/json', a.download, a.href].join(':')
    e.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null)
    a.dispatchEvent(e)
 }
})(console)


// This scans the game's memory for messages from the server in JSON format, parses them, and returns them as a list.
// Each block in memory looks like this: {"CommonResponse":{...},"Actual data":{...}}
function parse(arr=gameInstance.Module.HEAPU8) {
    const lbrac = 123;
    const rbrac = 125;
    const prefix = encoder.encode('"CommonResponse":')
    var blockStart;
    var depth = 0;
    var prefixIndex;
    var r = [];

    for (var i = 0; i < arr.length; i++) {
        // If we're not already in a block, we can skip ahead to the next left-bracket.
        if (depth == 0) {
            i = arr.indexOf(lbrac, i);
            if (i == -1) break; // If we can't find another left-bracket, there aren't any more blocks.
        }
        var elem = arr[i];
        if (elem == lbrac) {
            if (depth == 0) { // If it's the start of a new block...
                blockStart = i; // Remember where it starts...
                prefixIndex = 0; // And start comparing with the start of the prefix again.
            }
            depth++;
            continue;
        }
        // Blocks are assumed to not have any null characters in them.
        // So if there is one, or if it's near the start of a block and the prefix doesn't match, reset and skip to the next block.
        else if (elem == 0 || (prefixIndex < prefix.length && elem != prefix[prefixIndex])) {
            depth = 0;
            prefixIndex = null;
            continue;
        }
        else if (elem == rbrac) {
            depth--;
            if (depth == 0) { // If the block is over, convert it to UTF-8, parse it as JSON, and add it to the list.
                try {
                    r.push(JSON.parse(decoder.decode(arr.subarray(blockStart, i+1))));
                }
                catch (e) {}
                prefixIndex = null;
            }
            continue;
        }
        prefixIndex++;
    }
    // We want the responses to be in chronological order.
    return r.sort((a,b) => new Date(a.CommonResponse.ServerTime) - new Date(b.CommonResponse.ServerTime));
}

var _Rarity = {1: "N", 2: "R", 3: "SR", 4: "UR"};
Object.freeze(_Rarity);
var _OrbEffectNames = {"mhp":"HP", "atk": "ATK", "def":"DEF", "agi": "AGI","acc": "FCS", "res": "RES"}
Object.freeze(_OrbEffectNames);


function getCards(resps) {
    let cardsLists = resps.filter(e => e.CardGetUserCardListResponse);
    if(cardsLists.length == 0){
        console.error("Can't find dress data in the game memory");
        return;
    }
    return cardsLists[cardsLists.length-1].CardGetUserCardListResponse.UserCardList;
}

function getStoredCards(resps) {
    let cardsLists = resps.filter(e => e.CardContainerGetListResponse);
    if(cardsLists.length == 0){
        return [];
    }
    return cardsLists[cardsLists.length-1].CardContainerGetListResponse.UserContainerCardList;
}

function CardsToSheet(cards, storedCards=[]){
    return "id,dress,rarity,girl,element,LVL,LB,HP,ATK,DEF,FCS,RES,AGI,joker,s1,s2,s3,beast,bs1,bs2,uid\n" +
        cards.map(e=> CardToRow(e)).join("\n") + "\n" +
        (storedCards.length > 0 ? storedCards.map(e=> CardToRow(e)).join("\n") + "\n" : "");
}

function CardToRow(card){
    beastSkillList = card.BeastSkillList || []
    bs = [1,2].map(i => beastSkillList.filter(e => e.SkillNo == i)[0]?.SkillId?.slice(2) || "")

    return card.CardId + "," +
        card.Name + "," +
        _Rarity[card.Rarity] + "," +
        card.CharacterKind + "," +
        card.Element + "," +
        card.Lv.Lv + "," +
        ~~card.ArousalCount + "," +
        card.Mhp + "," +
        card.Atk + "," +
        card.Def + "," +
        card.Acc + "," +
        card.Res + "," +
        card.Agi + "," +
        (card.SubElementList.includes(7) || (card.IsJoker == true)) + "," +
        (card.SkillList[0]?.Lv || "") + "," +
        (card.SkillList[1]?.Lv || "") + "," +
        (card.SkillList[2]?.Lv || "") + "," +
        (card.IsUnlockedBeast == true) + "," +
        bs[0] + "," +
        bs[1] + "," +
        card.UserCardId
}

function getRunes(resps) {
    let runesLists = resps.filter(e => e.RuneGetUserRuneListResponse);
    if(runesLists.length == 0){
        console.error("Can't find orb data in the game memory");
        return;
    }
    return runesLists[runesLists.length-1].RuneGetUserRuneListResponse.UserRuneList;
}

function RunesToSheet(runes){
    if(runes.length == 0){
        console.error("Can't find orb data in the game memory");
        return;
    }
    return "uid,rarity,type,main_stat,HP,ATK,DEF,AGI,FCS,RES,HP%,ATK%,DEF%,ALL%,CC,CD,element,"+
        "ATK_up,DEF_up,gauge_up,"+
        "counter,drain,recover,"+
        "reb_HP,reb_gauge,"+
        "fire+,lightning+,water+,light+,dark+,"+
        "HP_ATK,HP_DEF,HP_AGI,"+
        "DMG_AGI,DMG_DEF,"+
        "CD-,fire-,lightning-,water-,light-,dark-\n"+
        runes.map(e=> RuneToRow(e)).join("\n");
}

function SubEffectsToRow(effects){
    let effTypes = [
        "mhp_add",
        "atk_add",
        "def_add",
        "agi_add",
        "acc_add",
        "res_add",
        "mhp_ratio",
        "atk_ratio",
        "def_ratio",
        "all_ratio",
        "critical_rate_add",
        "critical_damage_rate_add",
        "element_element",

        "atk_up_rate_add",
        "def_up_rate_add",
        "gauge_up_rate_add",

        "counter_damage_rate_add",
        "drain_rate_add",
        "hp_recovery_rate_add",

        "rebirth_hp_rate_add",
        "rebirth_gauge_up_rate_add",

        "element_2_damage_rate_add",
        "element_3_damage_rate_add",
        "element_4_damage_rate_add",
        "element_5_damage_rate_add",
        "element_6_damage_rate_add",

        "add_atk_by_hp_decrease_add",
        "add_def_by_hp_decrease_add",
        "add_agi_by_hp_decrease_add",

        "add_damage_by_agi_rate_add",
        "add_damage_by_def_rate_add",

        "critical_damage_rate_sub",
        "element_2_damage_rate_sub",
        "element_3_damage_rate_sub",
        "element_4_damage_rate_sub",
        "element_5_damage_rate_sub",
        "element_6_damage_rate_sub"
    ]

    return effTypes.map(x => effects[x] ?? '').join(',');
}

function RuneToRow(rune){
    let subeff = {};
    for (let e of rune.SubEffectList) {
        subeff[e.EffectKind] = e.Value;
    }

    return rune.UserRuneId + "," +
        _Rarity[rune.Rarity] + "," +
        _OrbEffectNames[rune.MainEffect.ParameterKind] + (rune.MainEffect.ValueKind == "ratio"? "%":"") + "," +
        rune.MainEffect.Value + "," +
        SubEffectsToRow(subeff)
}

function getSFMembers(resps){
    var memberdamage = resps.filter(e => e.TeamRaidGetTeamMemberListResponse).map(e => e.TeamRaidGetTeamMemberListResponse.MemberList).pop();
    if(memberdamage.length == 0){
        console.error("Can't find SF data in the game memory");
        return;
    }
    return memberdamage
}

function SFToSheet(memberdamage){
    return memberdamage.map(e => e.UserName).join('\t') + '\n' + memberdamage.map(e => e.TotalDamage).join('\t');
}

// If needed in the future.
function getBattleLogs(){
    battle_logs = resps.filter(e => e.BattleExecuteSkillResponse).map(e => e.BattleExecuteSkillResponse.BattleLog);
    if(battle_logs.length == 0){
        console.error("Can't find battle log data in the game memory");
        return;
    }
    return battle_logs
}


var fsbutton =  document.getElementById('fsbutton') || document.createElement('button');
fsbutton.id = "fsbutton";
fsbutton.innerHTML = '<p style="line-height:0.85em;">⇱&emsp;<br>&emsp;⇲</p>';
fsbutton.onclick = fullscreen;
(document.body || document.documentElement).appendChild(fsbutton);


async function messageListener(e) {
    let d = e.data;
    if (d.mgcm !== true || d.topage !== true) return;
    if (d.type === 'vars') {
        let vars = d.vars;
        for (let key of Object.keys(vars)) {
            window[key] = vars[key];
        }
    }
    if (d.type === 'gvars') {
        let vars = {};
        for (let key of d.vars) {
            vars[key] = window[key];
            e.source.postMessage({mgcm: true,  topage: false, id: d.id, response: vars})
        }
    }
    if (d.type === 'gamemessage') {
        postGameMessage(d.url, d.message, d.noBlock).then((x) => {
            e.source.postMessage({mgcm: true,  topage: false, id: d.id, response: x})
        });
    }
    if (d.type === 'gamemessages') {
        postGameMessages(d.pairs);
    }
}
window.removeEventListener('message', messageListener);
window.addEventListener('message', messageListener);


if (typeof(loadedObject) === 'undefined') var loadedObject = null;

function validateTeam(inObject, nUnits){
    return (async function () {
        let runeIDs = await postGameMessage('/Rune/GetUserRuneList', {}, true).then(message => message?.RuneGetUserRuneListResponse?.UserRuneList?.map(x => x?.UserRuneId));
        let cardIDs = await postGameMessage('/Card/GetUserCardList', {}, true).then(message => message?.CardGetUserCardListResponse?.UserCardList?.map(x => x?.UserCardId));
        if (!runeIDs || !cardIDs) return null;

        let out = [];
        try {
            for (let i = 0; i < nUnits; i++) {
                out[i] = {UnitNumber: i+1, DeckUnitSlotList: []};
                for (let j = 0; j < 5; j++) {
                    let slot = inObject[i]?.DeckUnitSlotList[j];

                    let runeId = slot?.UserRuneId;
                    if (!Number.isInteger(runeId)) {
                        runeId = 0;
                    } else if (!runeIDs.includes(runeId)) {
                        alert(`Team import: Orb ${runeId} missing.`);
                        return null;
                    }

                    let cardId = slot?.UserCardId;
                    if (!Number.isInteger(cardId)) {
                        cardId = 0;
                    } else if (!cardIDs.includes(cardId)) {
                        alert(`Team import: Dress ${cardId} missing or in storage.`);
                        return null;
                    }

                    out[i].DeckUnitSlotList[j] = {
                        UnitSlotNumber: j+1,
                        UserRuneId: runeId,
                        UserCardId: cardId
                    };
                }
            }
        } catch (error) {
            alert(error.message);
            return null;
        }
        return out;
    })().catch(log);
}

function dropHandler(ev){
    ev.preventDefault();
    return (async function () {
    if (ev.dataTransfer.items[0]?.kind === 'file') {
        let file = ev.dataTransfer.items[0].getAsFile();
        let text = await file.text();
        loadedObject = JSON.parse(text);
    }
    })()
}
document.getElementById("gameContainer").setAttribute("ondrop", "dropHandler(event);");

function dragOverHandler(ev) {
    ev.preventDefault();
}
document.getElementById("gameContainer").setAttribute("ondragover", "dragOverHandler(event);");

var sessid = Promise.resolve(magicami.sessid);
if (typeof(gameInstance.Module.nya_patched) !== 'undefined' && gameInstance.Module.nya_patched) {
    gameInstance.Module['wr_overrider'] = async function(request, payload){
        payload = new Uint8Array(payload);
        let url = gameInstance.Module.wr_urls[request];
        let r = payload;
        if (url.startsWith(document.location.origin) && !url.includes('wasm')) {
            let rsid;
            let sid = await sessid;

            const http = gameInstance.Module.wr.requestInstances[request];
            http.timeout = 0;
            const oldload = http.onload;
            http.onload = function (e) {
                rsid(http.getResponseHeader('magicami-sessid') ?? sid);
                return oldload(e);
            }

            if (url === document.location.origin + '/Deck/Save'){
                if (loadedObject) {
                    let decrypted = await decryptMessage(decoder.decode(payload));
                    let message = decrypted.message;
                    let nUnits;
                    if (message.DeckNumber > 400 && message.DeckNumber < 500)
                        nUnits = 5;
                    else if (message.DeckNumber > 300 && message.DeckNumber < 400)
                        nUnits = 3;
                    else
                        nUnits = 4
                    let loadedTeam = await validateTeam(loadedObject, nUnits);
                    if (loadedTeam) {
                        loadedObject = null;
                        message.DeckUnitList = loadedTeam;
                        let encrypted = await encryptMessage(sid, message);
                        r = encoder.encode(encrypted);
                    }
                }
            }

            sessid = new Promise(resolve => rsid = resolve);
        }
        return r;
    }
}

function postGameMessage(url, message={}, noBlock=false) {
    return (async function() {

    let rsid = null;
    let sid = await sessid;
    if (sid === null) {
        sid = magicami.sessid;
        sessid = Promise.resolve(sid);
    }
    if (!noBlock) sessid = new Promise(resolve => rsid = resolve);
    let body = await encryptMessage(sid, message)
    const init = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-type': 'application/json',
            'magicami-sessid': sid,
        },
        body: body
    };
    time = new Date().toLocaleString(undefined, {dateStyle: 'short', timeStyle: 'medium', hour12: false});
    const resp = await fetch(document.location.origin + url, init);
    const respBody = await resp.text();
    const decrypted = await decryptMessage(respBody);
    if (!noBlock) rsid(decrypted.sessid);
    const r = decrypted.message;
    window.postMessage({mgcm: true, topage: false, log: {'time': time, 'userID': magicami.userId, 'url': url, 'request': message, 'response': r}}, "*");
    return r
    })()
}

function postGameMessages(pairs) {
    if (pairs.length == 0) return;

    let {url, message} = pairs[0];
    postGameMessage(url, message)
    .then(() => postGameMessages(pairs.slice(1)))
    .catch(log);
}

function fetchCards() {
    return postGameMessage('/CardContainer/GetList')
    .then(message => message?.CardContainerGetListResponse?.UserCardList.concat(message?.CardContainerGetListResponse?.UserContainerCardList))
    .catch(log);
}

function fetchRunes() {
    return postGameMessage('/Rune/GetUserRuneList')
    .then(message => message?.RuneGetUserRuneListResponse?.UserRuneList)
    .catch(log);
}

function fetchSFDamage() {
    return postGameMessage('/TeamRaid/')
    .then(x => x.TeamRaidResponse.QuestId)
    .then(questid => postGameMessage('/TeamRaid/GetTeamMemberList', {QuestId: questid}))
    .then(message => message?.TeamRaidGetTeamMemberListResponse?.MemberList)
    .catch(log);
}


var frameWorker = new Worker(URL.createObjectURL(new Blob([`onmessage = (e) => {
    if (self.interval)
        clearInterval(self.interval);
    if (e.data == 0)
        return;
    const delay = 1000/e.data;
    self.interval = setInterval(postMessage, delay, null);
}`], {type: 'application/javascript'})));

frameWorker.onmessage = function() {
    gameInstance.Module.Browser.mainLoop.currentFrameNumber = gameInstance.Module.Browser.mainLoop.currentFrameNumber + 1 | 0;
    gameInstance.Module.Browser.mainLoop.runIter((function() {gameInstance.Module["dynCall_v"](gameInstance.Module.Browser.mainLoop.func)}));
};

var bgFPS = 10;
function visibilityListener() {
    if (document.visibilityState === 'visible') {
        frameWorker.postMessage(0);
    } else {
        frameWorker.postMessage(bgFPS);
    }
}
document.addEventListener("visibilitychange", visibilityListener);


window.postMessage({mgcm: true, topage: false, userId: magicami.userId}, "*")
postGameMessage('/QuestSelector/GetQuestList');

}
