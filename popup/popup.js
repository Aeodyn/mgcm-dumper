const _ = browser.i18n.getMessage;
const bgPage = browser.extension.getBackgroundPage();

// From https://github.com/erosman/HTML-Internationalization
document.querySelectorAll('[data-i18n]').forEach(node => {
	let [text, attr] = node.dataset.i18n.split('|');
	text = _(text);
	attr ? node.setAttribute(attr, text) : node.append(text);
});

function send(message) {
	browser.tabs.query({active: true, currentWindow: true}).then(result => {
		result.forEach(tab => {
			bgPage.ports[tab.id].postMessage(message);
		});
	});
}

function updateSFBoss() {
	browser.tabs.query({active: true, currentWindow: true}).then(tabs => {
		for (tab of tabs) {
			let sfData = bgPage.sfData[tab.id];
			if (sfData) {
				document.getElementById('dump-sf').hidden = false;
				document.getElementById('sf').hidden = false;
				document.getElementById('sf-round').textContent = sfData.round;
				document.getElementById('sf-name').textContent = sfData.name;
				document.getElementById('sf-hp').textContent = sfData.hp;
			}
		}
	});
}

function updateMessageListener(request, sender, sendResponse) {
	if (request === 'update-popup') {
		updateSFBoss();
	}
}


function clickListener(e) {
	switch (e.target.id) {
		case "dump-dresses":
			send("dump-dresses");
			break;
		case "dump-orbs":
			send("dump-orbs");
			break;
		case "dump-both":
			send("dump-both");
			break;
		case "dump-sf":
			send("dump-sf");
			break;
	}
}
document.removeEventListener("click", clickListener);
document.addEventListener("click", clickListener);



browser.tabs.query({active: true, currentWindow: true}).then(tabs => {
	for (tab of tabs) {
		if (bgPage.features.sfBoss && bgPage.questList?.[tab.id]?.includes("8")) {
			document.getElementById('sf').hidden = false;
			browser.runtime.onMessage.addListener(updateMessageListener);
			if (tab.id in bgPage.ports) {
				bgPage.ports[tab.id].postMessage({type: 'gamemessage', url: '/TeamRaid/', message: {}});
			}
		}
	}
});
