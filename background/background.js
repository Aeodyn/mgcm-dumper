var ports = {};
var features = {};
var sfData = {};
var questList = {};
var pollers = {};
var lastGameMessageID = 0;
var userIDs = {};

function connectListener(p) {
	ports[p.sender.tab.id] = p;
	p.onDisconnect.addListener(disconnectListener);

	let uIDListener = (m) => {
		if (m.userId) {
			userIDs[p.sender.tab.id] = m.userId;
			p.onMessage.removeListener(uIDListener);
		}
	};
	p.onMessage.addListener(uIDListener);
}

function disconnectListener(p) {
	delete ports[p.sender.tab.id];
	delete userIDs[p.sender.tab.id];
}

function broadcast(message) {
	for (let i in ports) {
		ports[i].postMessage(message);
	}
}

function postGameMessage(port, url, message, noBlock=false) {
	let id = lastGameMessageID++;
	let promise = new Promise((resolve, reject) => {
		let listener = (m) => {
			if (id == m.id) {
				resolve(m.response);
				port.onMessage.removeListener(listener);
			}
		};
		port.onMessage.addListener(listener);
	});
	port.postMessage({type: 'gamemessage', id: id, url: url, message: message, noBlock: noBlock});
	return promise;
}

function postGameMessages(port, pairs) {
    if (pairs.length == 0) return;

    let {url, message} = pairs[0];
    postGameMessage(port, url, message)
    .then(() => postGameMessages(port, pairs.slice(1)))
    .catch(log);
}

async function frameworkListener(details) {
	if (!details.documentUrl.match(/^https:\/\/[^.\/]*.magicami/)) return;
	log('Framework caught.');

	let patches = fwPatches.perf.concat(fwPatches.wr, fwPatches.raf, Object.keys(features.fw || {}).flatMap(x => fwPatches?.[x]));

	let filter = browser.webRequest.filterResponseData(details.requestId);
	let data = [];

	filter.ondata = event => {
		data.push(new Uint8Array(event.data));
	}
	filter.onstop = async function (event) {
		let array = new Uint8Array(data.reduce((a, x) => a + x.length, 0));
		let i = 0;
		for (let buffer of data) {
			array.set(buffer, i);
			i += buffer.length;
		}

		let decompressed = decoder.decode(brotli.decompress(array));
		if (decompressed.length == 0) decompressed = decoder.decode(array);
		let newstr = replaceMulti(decompressed, patches);

		filter.write(encoder.encode(newstr));
		filter.close();
		log('Framework patched.');
	}
}

function loadedListener(details) {
	ports[details.tabId].postMessage('inject');

	setTimeout(() => {
		if (features.omnis?.poll.interval > 0) postOmnisRequest();
		if (features.bellShop?.poll.interval > 0) postBellShopRequest();
		if (features.surveys?.poll.interval > 0) broadcastDoSurveys();
	}, 5000);
}

function parseMessage(details) {
	let filter = browser.webRequest.filterResponseData(details.requestId);
	let data = [];

	filter.ondata = event => {
		data.push(event.data);
		filter.write(event.data);
	}
	return new Promise((resolve, reject) => {
		filter.onstop = async function (event) {
			let decoder = new TextDecoder("UTF-8");
			let str = "";
			for (let buffer of data) {
				str += decoder.decode(buffer, {stream: true});
			}
			str += decoder.decode(); // Ends the stream

			let {sessid, message} = await decryptMessage(str);
			filter.close();
			resolve(message);
		}
	});
}

function modifyMessage(details, func) {
	let filter = browser.webRequest.filterResponseData(details.requestId);
	let data = [];

	filter.ondata = event => {
		data.push(new Uint8Array(event.data));
	}
	filter.onstop = async function (event) {
		try {
			let array = new Uint8Array(data.reduce((a, x) => a + x.length, 0));
			let i = 0;
			for (let buffer of data) {
				array.set(buffer, i);
				i += buffer.length;
			}

			let str = decoder.decode(array);
			let {sessid, message} = await decryptMessage(str);
			let newmessage = await Promise.resolve(func(message));
			var newstr = await encryptMessage(sessid, newmessage);
		} catch (e) {
			console.error(`Error modifying request to ${details.url}: `, e);
			for (let buffer of data) {
				filter.write(buffer);
				filter.close();
				return;
			}			
		}
		filter.write(encoder.encode(newstr));
		filter.close();
	}
}

function storageListener(changes, area) {
	if ('features' in changes) {
		for (let key in features) {
			delete features[key];
		}
		for (let [key, value] of Object.entries(changes['features'].newValue)) {
			features[key] = value;
		}
		for (let key of ['sfBoss', 'omnis', 'bellShop', 'stam', 'surveys']) {
			if (features[key] && !features[key].enabled) {
				delete features[key];
			} else {
				features[key].poll.interval = parseInterval(features[key].poll.interval);
				features[key].poll.jitter /= 100;
			}
		}
		if (features.repl && !features.repl.enabled) {
			delete features.repl;
		}
		if (features.redirect && !features.redirect.enabled) {
			delete features.redirect;
		}
		if (features.log && !features.log.enabled) {
			delete features.log;
		}


		updateSFBossListener();
		updateSFPolling();
		updateOmnisPolling();
		updateShopListener();
		updateBellShopPolling();
		updateStamPolling();
		updateSurveysPolling();
		updateAllMessageListener();
		updateOsapiListener();
		broadcast({type: 'vars', vars: {bgFPS: +features.bgFPS}});
	}
}

async function osapiListener(details) {
	let base;
	if (features.redirect.platform === 'en' && !details.url.match(/en-api-dx\.magicami\.johren\.games/))
		base = `https://osapi.johren.net/gadgets/ifr?synd=johren&container=johren&owner=jrn-2600633&viewer=jrn-2600633&aid=jrn-1688&mid=1&country=US&lang=en&view=canvas&parent=https%3A%2F%2Fwww.johren.games&url=https%3A%2F%2Fen-api-dx.magicami.johren.games%2Fgadget%2Fgadget_en.xml&noinfo=0&nocache=0&debug=1&view-params=&st=`
	else if (features.redirect.platform === 'zh' && !details.url.match(/tw-api-dx\.magicami\.johren\.games/))
		base = `https://osapi.johren.net/gadgets/ifr?synd=johren&container=johren&owner=jrn-2600633&viewer=jrn-2600633&aid=jrn-90950&mid=1&country=US&lang=en&view=canvas&parent=https%3A%2F%2Fwww.johren.games&url=https%3A%2F%2Ftw-api-dx.magicami.johren.games%2Fgadget%2Fgadget_tw.xml&noinfo=0&nocache=0&debug=1&view-params=&st=`
	else if (features.redirect.platform === 'n' && !details.url.match(/nutaku/))
		base = `https://osapi.nutaku.com/gadgets/ifr?synd=nutaku&container=nutaku&country=US&lang=en&mid=1&owner=103245267&viewer=103245267&aid=83408&tid=mgcm-dx&view=canvas&parent=https%3A%2F%2Fwww.nutaku.net&noinfo=0&nocache=0&debug=1&view-params=&st=`
	else
		return;

	let st = details.url.match(/st=(.*)/)[1];
	if (!st) {
		log('Error extracting "st" from osapi URL.');
		return
	}
	if (features.redirect.platform === 'en')
		log('Redirected to Johren EN');
	if (features.redirect.platform === 'zh')
		log('Redirected to Johren ZH');
	if (features.redirect.platform === 'n')
		log('Redirected to Nutaku');
	return {redirectUrl: base+st}
}

function stringReplListener(details) {
	if (features.repl?.collab) {
		modifyMessage(details, function(x) {
			replaceObjs(x, stringRepl.collab);
			return x;
		});
	}
}

const stringReplPaths = [
	'Card/*',
	'Deck/*',
	'QuestBattle/GetBattleData',
	'Sabbath/*',
	'SabbathBattle/GetBattleData',
	'SoloRaid/*',
	'BoxGachaDamageRanking/*',
	'CardContainer/*',
	'Gacha/*'
]

browser.webRequest.onBeforeRequest.addListener(
	stringReplListener,
	{urls: stringReplPaths.flatMap(x => ['https://*.magicami.johren.games/' + x, 'https://*.magicami.net/' + x])},
	["blocking"]
);

browser.runtime.onConnect.addListener(connectListener);

browser.webRequest.onBeforeRequest.addListener(
	frameworkListener,
	{urls: [
		"https://*.magicami.johren.games/Build/mgcm_en.framework.js.unityweb?v=*",
		"https://*.magicami.net/Build/magicami.framework.js.unityweb?v=*"
	]},
	["blocking"]
);

function updateOsapiListener() {
	if (features.redirect?.enabled) {
		let p = features.redirect?.platform;
		let match;
		if (p === 'en' || p === 'zh')
			match = 'https://osapi.nutaku.com/gadgets/ifr?synd=nutaku&container=nutaku&*mgcm*'
		if (p === 'n')
			match = 'https://osapi.johren.net/gadgets/ifr?synd=johren&container=johren*magicami*'

		browser.webRequest.onBeforeRequest.addListener(
			osapiListener,
			{urls: [match]},
			['blocking']
		)
	} else {
		browser.webRequest.onBeforeRequest.removeListener(osapiListener);
	}
}

// This request is used as an indicator that the game is done loading.
browser.webRequest.onBeforeRequest.addListener(
	loadedListener,
	{urls: ["https://*.magicami.johren.games/Master/GetRawAll", "https://*.magicami.net/Master/GetRawAll"]}
);

browser.storage.local.get('features').then(x => {
	if (!x?.['features']) return;

	for (let [key, value] of Object.entries(x['features'])) {
		features[key] = value;
	}
	for (let key of ['sfBoss', 'omnis', 'bellShop', 'stam', 'surveys']) {
		if (features[key] && !features[key].enabled) {
			delete features[key];
		} else {
			features[key].poll.interval = parseInterval(features[key].poll.interval);
			features[key].poll.jitter /= 100;
		}
	}
	if (features.repl && !features.repl.enabled) {
		delete features.repl;
	}

	updateSFBossListener();
	updateSFPolling();
	updateOmnisPolling();
	updateShopListener();
	updateBellShopPolling();
	updateStamPolling();
	updateSurveysPolling();
	updateAllMessageListener();
	updateOsapiListener();
	broadcast({type: 'vars', vars: {bgFPS: +features.bgFPS}});
});

browser.storage.onChanged.addListener(storageListener);
