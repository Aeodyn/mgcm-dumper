const elements = {
	'2': 'fire',
	'3': 'lightning',
	'4': 'water',
	'5': 'light',
	'6': 'dark'
}

const orbTypes = {
	2: 'mhp_ratio',
	3: 'mhp_add',
	4: 'atk_ratio',
	5: 'atk_add',
	6: 'def_ratio',
	7: 'def_add',
	8: 'agi_add',
	9: 'acc_add',
	10: 'res_add',
	11: 'elem'
}


function checkItem(item) {
	if (item.IsSoldOut || item.LeftLimitCount == 0 ) return false;

	const options = features.bellShop;

	if (!item.DisplayMouko) return false;

	switch (item.DisplayMouko.MoukoKind) {
	case 'rune_101': // Orb
		return !!options?.orb?.[item.RuneDetail.Rarity]?.[item.RuneDetail.MainEffect.EffectKind];

	case 'item_107': // Yarn
		return !!options?.yarn?.[item.DisplayMouko.MoukoId];

	case 'item_108': // Crystals
		const [type, rarity] = item.DisplayMouko.MoukoId;
		return !!options?.crystal?.[rarity]?.[elements[type]];

	case 'item_111': // Ticket
		return !!options?.ticket?.blue

	case 'item_112': // Ticket shards
		return !!options?.ticket?.gold

	case 'item_125': // Orb fragments
		return !!options?.frag?.[orbTypes[+item.DisplayMouko.MoukoId]];

	case 'item_128': // Orb dust
		return !!options?.dust?.[orbTypes[+item.DisplayMouko.MoukoId]];

	case 'item_133': // Orb shards
		return !!options?.shard?.[orbTypes[(+item.DisplayMouko.MoukoId)+1]];
	}

	if (item.GoodsAssetName.includes('treasure_map') && !item.GoodsAssetName.includes('already_acquired')) {
		return !!options?.clam;
	}
}

function postBellShopRequest() {
	log(`Checking the bell shop.`);
	broadcast({type: 'gamemessage', url: '/Shop/GetShopGoodsList', message: {'ShopKind': 1, 'ShopId': '1_1', 'TabKind': ''}});
}

function updateBellShopPolling() {
	if (pollers.bellShop) {
		stopPoll(pollers.bellShop);
		delete pollers.bellShop;
	}
	if (features.bellShop?.poll.interval > 0) {
		pollers.bellShop = startPoll(features.bellShop.poll.interval * 1000, features.bellShop.poll.jitter, postBellShopRequest);
	}
}

function shopListener(details) {
	modifyMessage(details, async function(x) {
		if (!x?.ShopGetShopGoodsListResponse?.BellShopInfo) return x;

		const items = x.ShopGetShopGoodsListResponse.ShopTabList[0].ShopGoodsList;
		const purchase = items.filter(checkItem);
		let bells = x.CommonResponse.Header.Bell.Amount;
		for (item of purchase) {
			bells -= item.ConsumeMoukoList[0].ConsumeMoukoAmount;
			if (bells > 0) {
				log(`Tab ${details.tabId}: Buying ${item.ShopGoodsId}: ${item.GoodsName}`);
				ports[details.tabId].postMessage({type: 'gamemessage', url: '/Shop/PurchaseShopGoods', message: {ShopKind: 1, ShopId: '1_1', ShopGoodsId: item.ShopGoodsId, Amount: 1}});
				item.IsSoldOut = true;
			} else {
				log(`Tab ${details.tabId}: Not enough bells to buy ${item.ShopGoodsId}: ${item.GoodsName}`);
				break;
			}
		}
		return x;
	});
}

function updateShopListener() {
	if (features.bellShop) {
		browser.webRequest.onBeforeRequest.addListener(
			shopListener,
			{urls: ['https://*.magicami.johren.games/Shop/GetShopGoodsList', 'https://*.magicami.net/Shop/GetShopGoodsList']},
			["blocking"]
		);
	} else {
		browser.webRequest.onBeforeRequest.removeListener(shopListener);
	}
}
