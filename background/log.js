function allMessageListener(details) {
	const time = new Date().toLocaleString(undefined, logDateFormat);
	(async function () {
		let request;
		if (details.requestBody && details.requestBody.raw.length > 0) {
			request = (await decryptMessage(decoder.decode(details.requestBody.raw[0].bytes))).message;
		} else {
			request = null;
		}
		const response = await parseMessage(details);
		logPersistent({'time': time, 'userID': userIDs[details.tabId], 'url': details.url, 'request': request, 'response': response})
	})();
	return null
}

function logPortListener(m) {
	if (m.log)
		logPersistent(m.log);
}

function updateAllMessageListener() {
	if (features?.log?.all) {
		browser.webRequest.onBeforeRequest.addListener(
			allMessageListener,
			{urls: ["https://*.magicami.johren.games/*", "https://*.magicami.net/*"]},
			['requestBody']
		);
		for (let i in ports) {
			ports[i].onMessage.addListener(logPortListener);
		}
	} else {
		browser.webRequest.onBeforeRequest.removeListener(allMessageListener);
		for (let i in ports) {
			ports[i].onMessage.removeListener(logPortListener);
		}
	}
}
