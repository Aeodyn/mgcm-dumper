const surveyBlacklists = {};
const inProgressSurveys = {};

async function fetchInBattle(port) {
	let resp = await postGameMessage(port, '/BattleExistence/GetNotFinished');
	let t = resp.BattleExistenceGetNotFinishedResponse;
	if (t === undefined) {
		log('Bad /BattleExistence/GetNotFinished response');
		throw resp;
	}
	return (t.Quest || t.Sabbath || t.TeamSabbath || t.ArenaSabbath)
}

async function fetchSurveyStatus(port) {
	let resp = await postGameMessage(port, '/TreasureMap');
	let t = resp.TreasureMapTopResponse;
	if (t === undefined) {
		log('Bad /TreasureMap response');
		throw resp;
	}
	return t
}

async function fetchAvailSurveys(port) {
	let resp = (await postGameMessage(port, '/TreasureMap/GetSearchableStageList', {'SlotKind': '1'}, true));
	let t = resp.TreasureMapGetSearchableStageListResponse;
	if (t === undefined || t.SearchableStageList === undefined) {
		log('Bad /TreasureMap/GetSearchableStageList response');
		throw resp;
	}
	return t.SearchableStageList.filter(x => x.Stage.ConcreteAmount)
}

async function fetchSharedSurveys(port) {
	let resp = (await postGameMessage(port, '/TreasureMap/GetSearchableStageList', {'SlotKind': '2'}, true));
	let t = resp.TreasureMapGetSearchableStageListResponse;
	if (t === undefined || t.SearchableStageList === undefined) {
		log('Bad /TreasureMap/GetSearchableStageList response');
		throw resp;
	}
	return t.SearchableStageList
}

async function postStartSurvey(port, stage, recTeam=true) {
	if (recTeam) {
		let teamResp = await postGameMessage(port, '/TreasureMap/GetRecommendedDeck', {
		'QuestStageRequest': {
			'QuestId': stage.QuestId,
			'DungeonId': stage.DungeonId,
			'StageId': stage.StageId
		}});
		if (teamResp.ErrorResponse)
			if (teamResp.ErrorResponse.ErrorCode === 35007)
				return false;
			else {
				log('Bad /TreasureMap/GetRecommendedDeck response')
				throw teamResp;
			}
	}

	let sInfo = (await postGameMessage(port, '/TreasureMap/GetStageInfo', {
		'QuestStageRequest': {
			'QuestId': stage.QuestId,
			'DungeonId': stage.DungeonId,
			'StageId': stage.StageId
		},
		'TreasureMapStageInstanceRequest': {
			'StageInstanceId': stage.StageInstanceId,
			'TreasureMapKind': stage.TreasureMapKind
		}
	}, true)).TreasureMapGetStageInfoResponse;

	if (!sInfo.IsSearchEnable)
		return false;

	postGameMessage(port, '/TreasureMap/StartSearch', {
		'QuestStageRequest': {
			'QuestId': sInfo.QuestId,
			'DungeonId': sInfo.DungeonId,
			'StageId': sInfo.StageId
		},
		'TreasureMapStageInstanceRequest': {
			'StageInstanceId': sInfo.StageInstanceId,
			'TreasureMapKind': sInfo.TreasureMapKind
		},
		'TreasureMapSlotRequest': {
			'SlotKind': '',
			'SlotNumber': ''
		}
	});
	return true
}

async function postFinishSurvey(port, id) {
	let resp = await postGameMessage(port, '/TreasureMap/FinishSearch', {'UserSearchId': id,'IsForceFinish': false});
	let t = resp.TreasureMapFinishSearchResponse;
	if (t === undefined) {
		log('Bad /TreasureMap/FinishSearch response');
		throw resp;
	}
	return t
}

function chooseSurvey(stages, minLvl=1) {
	// TODO: Priorities
	let specialStages = ['13_1_131', '13_1_132', '13_1_133']
	let filtered = stages.filter(x => x.Stage.StageLevel >= minLvl);

	filtered = filtered.filter(x => (x.Stage.SuccessRewardList === undefined) || x.Stage.SuccessRewardList.some(y => y.CardDetail) || specialStages.includes(x.DungeonId));

	if (filtered.length == 0)
		return null;
	return filtered[Math.floor(Math.random()*filtered.length)];
}

async function finishSurveys(port) {
	if (await fetchInBattle(port))
		return;
	let status = await fetchSurveyStatus(port);
	let tabID = Object.keys(ports)[Object.values(ports).indexOf(port)];
	if (!(tabID in surveyBlacklists))
		surveyBlacklists[tabID] = new Set();
	if (!(tabID in inProgressSurveys))
		inProgressSurveys[tabID] = new Set();

	let sStatus = status.SharedSearchSlotList?.[0];
	if (sStatus && status.SharedMapRemainSearchCount && sStatus.SuccessProbability && sStatus.IsSearchFinished) {
		try {
			let sFinish = await postFinishSurvey(port, sStatus.UserSearchId);
			if (sFinish.IsSuccess) {
				log(`Shared survey ${sFinish.StageName} succeeded. Rewards:`);
				log(sFinish.SuccessRewardList);
			} else
				log(`Shared survey ${sFinish.StageName} failed.`);
			delete inProgressSurveys[tabID][sStatus.UserSearchId]
		} catch (e) {
			let stage = inProgressSurveys[tabID][sStatus.UserSearchId];
			surveyBlacklists[tabID].add(stage);
			log(`Error finishing shared survey ${stage}.`);
			log(e);
		}
		status = await fetchSurveyStatus(port);
		sStatus = status.SharedSearchSlotList?.[0];
	}
	
	for (let i = 0; i < 3; i++) {
		sStatus = status.SearchSlotList[i];
		if (sStatus.IsLocked)
			continue;
		if (sStatus.SuccessProbability && sStatus.IsSearchFinished) {
			try {
				let sFinish = await postFinishSurvey(port, sStatus.UserSearchId);
				if (sFinish.IsSuccess) {
					log(`Survey ${sFinish.StageName} succeeded. Rewards:`);
					log(sFinish.SuccessRewardList);
				} else
					log(`Survey ${sFinish.StageName} failed.`);
				delete inProgressSurveys[tabID][sStatus.UserSearchId];
			} catch (e) {
				let stage = inProgressSurveys[tabID][sStatus.UserSearchId];
				surveyBlacklists[tabID].add(stage);
				log(`Error finishing shared survey ${stage}.`);
				log(e);
			}
			status = await fetchSurveyStatus(port);
			sStatus = status.SearchSlotList[i];
		}
	}
}

async function startSurveys(port) {
	let tabID = Object.keys(ports)[Object.values(ports).indexOf(port)];
	if (!(tabID in surveyBlacklists))
		surveyBlacklists[tabID] = new Set();
	if (!(tabID in inProgressSurveys))
		inProgressSurveys[tabID] = new Set();
	let bl = surveyBlacklists[tabID];
	let iPS = inProgressSurveys[tabID];
	let status = await fetchSurveyStatus(port);

	let sStatus = status.SharedSearchSlotList?.[0];
	if (sStatus && status.SharedMapRemainSearchCount && !sStatus.SuccessProbability) {
		let avail = (await fetchSharedSurveys(port)).filter(x => !bl.has(x.StageId));
		while (avail.length) {
			let survey = chooseSurvey(avail, +features.surveys?.shared_min_lvl);
			if (!(survey && survey.Stage))  // No surveys left?
				break;
			avail = avail.filter(x => x.StageId !== survey.StageId);
			if (await postStartSurvey(port, survey.Stage)) {
				log(`Starting shared survey ${survey.DungeonName} lvl ${survey.Stage.StageLevel}`);
				status = await fetchSurveyStatus(port);
				iPS[Math.max(...status.SearchSlotList.map(x => x.UserSearchId))] = survey.StageId;
				break
			} else {
				log(`Can't do shared survey ${survey.DungeonName} lvl ${survey.Stage.StageLevel}`);
				bl.add(survey.StageId)
			}
		}
	}
	
	let avail = (await fetchAvailSurveys(port)).filter(x => !bl.has(x.StageId));

	for (let i = 0; i < 3; i++) {
		sStatus = status.SearchSlotList[i];
		if (sStatus.IsLocked || sStatus.SuccessProbability)  // Locked or occupied
			continue;
		while (avail.length) {
			let survey = chooseSurvey(avail);
			if (!(survey && survey.Stage))  // No surveys left?
				break;
			avail = avail.filter(x => x.StageId !== survey.StageId);
			if (await postStartSurvey(port, survey.Stage)) {
				log(`Starting survey ${survey.DungeonName} lvl ${survey.Stage.StageLevel}`);
				status = await fetchSurveyStatus(port);
				iPS[Math.max(...status.SearchSlotList.map(x => x.UserSearchId))] = survey.StageId;
				break
			} else {
				log(`Can't do survey ${survey.DungeonName} lvl ${survey.Stage.StageLevel}`);
				bl.add(survey.StageId)
			}
		}
	}
}

async function doSurveys(port) {
	await finishSurveys(port);
	await startSurveys(port);
}

function broadcastDoSurveys() {
	log('Checking surveys.');
	for (let i in ports) {
		doSurveys(ports[i]);
	}
}

function updateSurveysPolling() {
	if (pollers.surveys) {
		stopPoll(pollers.surveys);
		delete pollers.surveys;
	}
	if (features.surveys?.poll.interval > 0) {
		pollers.surveys = startPoll(features.surveys.poll.interval * 1000, features.surveys.poll.jitter, broadcastDoSurveys);
	}
}