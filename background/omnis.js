function postOmnisRequest() {
	log('Collecting omnis jewels and bells.');
	broadcast({type: 'gamemessages', pairs: [{url: '/UserAbility', message: {}}, {url: '/UserAbility/ReceiveAll', message: {}}]});
}

function updateOmnisPolling() {
	if (pollers.omnis) {
		stopPoll(pollers.omnis);
		delete pollers.omnis;
	}
	if (features.omnis?.poll.interval > 0) {
		pollers.omnis = startPoll(features.omnis.poll.interval * 1000, features.omnis.poll.jitter, postOmnisRequest);
	}
}
