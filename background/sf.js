function postSFBoss(sfData) {
	let replacements = [
		['viewform?', 'formResponse?'],
		['usp=pp_url&', ''],
		['=club', `=${sfData.club}`],
		['=round', `=${sfData.round}`],
		['=hp', `=${sfData.hp}`],
		['=boss', `=${sfData.boss}`]
	]
	let url = replaceMulti(features.sfBoss.spreadsheet, replacements);
	let xhr = new XMLHttpRequest();
	xhr.open('POST', url);
	xhr.send();
}

function extractSFData(data) {
	var r = {}
	r.time = data.CommonResponse.ServerTime;
	r.club = data.TeamRaidResponse.TeamId;
	r.round = data.TeamRaidResponse.CurrentLapCount;
	for (let s of data.TeamRaidResponse.StageList) {
		r.hp = s.Boss.CurrentHp;
		if (r.hp > 0) {
			r.boss = s.Boss.Number;
			r.name = s.Boss.Name;
			break;
		}
	}
	return r;
}

function sfBossListener(details) {
	parseMessage(details).then(x => {
		let tSfData = extractSFData(x);
		sfData[details.tabId] = tSfData;
		browser.runtime.sendMessage('update-popup');
		if (features.sfBoss?.spreadsheet) {
			postSFBoss(tSfData);
		}
	});
}

function questListListener(details) {
	parseMessage(details).then(x => {
		questList[details.tabId] = x.QuestSelectorGetQuestListResponse.QuestInformationList.map(e => e.QuestKind);
	});
}

function updateSFBossListener() {
	if (features.sfBoss) {
		browser.webRequest.onBeforeRequest.addListener(
			sfBossListener,
			{urls: ["https://*.magicami.johren.games/TeamRaid/", "https://*.magicami.net/TeamRaid/"]},
			["blocking"]
		);
	} else {
		browser.webRequest.onBeforeRequest.removeListener(sfBossListener);
	}
}

function postSFBossRequest() {
	for (tabid in questList) {
		if (questList[tabid].includes('8')) {
			log(`Tab ${tabid}: Checking SF status.`);
			ports[tabid].postMessage({type: 'gamemessage', url: '/TeamRaid/', message: {}});
		}
	}
}

function updateSFPolling() {
	if (pollers.sfBoss) {
		stopPoll(pollers.sfBoss);
		delete pollers.sfBoss;
	}
	if (features.sfBoss?.poll.interval > 0) {
		pollers.sfBoss = startPoll(features.sfBoss.poll.interval * 1000, features.sfBoss.poll.jitter, postSFBossRequest);
		browser.webRequest.onBeforeRequest.addListener(
			questListListener,
			{urls: ["https://*.magicami.johren.games/QuestSelector/GetQuestList", "https://*.magicami.net/QuestSelector/GetQuestList"]},
			["blocking"]
		);
		broadcast({type: 'gamemessage', url: '/QuestSelector/GetQuestList', message: {}});
	} else {
		browser.webRequest.onBeforeRequest.removeListener(questListListener);
	}
}
