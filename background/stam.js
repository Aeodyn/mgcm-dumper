function broadcastStamRequest() {
	log('Checking stam.');
	for (let i in ports) {
		postStamRequest(i);
	}
}

async function postStamRequest(i) {
	const response = await postGameMessage(ports[i], '/Item/GetUserItemList', {'MoukoKind': 'item_104'});
	const stam0 = response.CommonResponse.Header.QuestStamina.Stamina;
	if (stam0 > features.stam.threshold) return;
	const stamMax = response.CommonResponse.Header.QuestStamina.RecoverMax;
	let pots0 = response.ItemGetUserItemListResponse.ItemList.filter(x => x.MoukoId === '1')?.[0]?.Amount ?? 0;
	if (pots0 > 0) {
		const n = Math.min(Math.ceil((Math.min(features.stam.target, stamMax) - stam0)/20), pots0);
		const pairs = Array(n).fill({url: '/Item/UseStaminaItem', message: {'MoukoId': '1'}});

		log(`Tab ${i}: Spending ${n} 20-stam pots.`);
		postGameMessages(ports[i], pairs);
	} else {
		pots0 = response.ItemGetUserItemListResponse.ItemList.filter(x => x.MoukoId === '2')?.[0]?.Amount ?? 0;
		if (pots0 == 0) {
			log('Out of pots.');
			return;
		}
		const n = Math.min(Math.ceil((Math.min(features.stam.target, stamMax) - stam0)/40), pots0);
		const pairs = Array(n).fill({url: '/Item/UseStaminaItem', message: {'MoukoId': '2'}});

		log(`Tab ${i}: Spending ${n} 40-stam pots.`);
		postGameMessages(ports[i], pairs);
	}
}

function updateStamPolling() {
	if (pollers.stam) {
		stopPoll(pollers.stam);
		delete pollers.stam;
	}
	if (features.stam?.poll.interval > 0) {
		pollers.stam = startPoll(features.stam.poll.interval * 1000, features.stam.poll.jitter, broadcastStamRequest);
	}
}
