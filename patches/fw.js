const fwPatches = {
perf: [
	["syncInternal: 1e3,",
	 "syncInternal: 6e4,"],

	["FS.syncfs(false,(function(err){fs.syncInProgress=false}));",
	 "if(FS.syncFSRequests==0){FS.syncfs(false,(function(err){fs.syncInProgress=false}));}"],

	["normalize:(function(path){var isAbsolute=path.charAt(0)===\"/\",trailingSlash=path.substr(-1)===\"/\";path=PATH.normalizeArray(path.split(\"/\").filter((function(p){return!!p})),!isAbsolute).join(\"/\");if(!path\&\&!isAbsolute){path=\".\"}if(path\&\&trailingSlash){path+=\"/\"}return(isAbsolute?\"/\":\"\")+path})",
	 "normalize:(function(path){if(path[0]===\"/\"\&\&path[1]===\"/\")return path.slice(1);return path})"],

	["join2:(function(l,r){return PATH.normalize(l+\"/\"+r)})",
	 "join2:(function(l,r){if(l===\"/\")return\"/\"+r;return l+\"/\"+r})"],

	["{return\"\"}resolvedPath=path+\"/\"+resolvedPath;resolvedAbsolute=path.charAt(0)===\"/\"}resolvedPath=PATH.normalizeArray(resolvedPath.split(\"/\").filter((function(p){return!!p})),!resolvedAbsolute).join(\"/\");return(resolvedAbsolute?\"/\":\"\")+resolvedPath||\".\"})",
	 "{return\"\"}if(resolvedPath===\"\"){resolvedPath=path}else{resolvedPath=path+\"/\"+resolvedPath};resolvedAbsolute=path.charAt(0)===\"/\"}return resolvedPath})"],

	["loadLocalEntry:(function(path,callback){var stat,node;",
	 "loadLocalEntry:(function(path,callback){if(path.length>51){callback(null,{});return}var stat,node;"],

	["storeLocalEntry:(function(path,entry,callback){try{if",
	 "storeLocalEntry:(function(path,entry,callback){if(path.length>51){callback(null);return}try{if"],

	["var nodeName=node.name;if(node.parent.id===parent.id\&\&nodeName===name){return node}",
	 "if(node.parent.id===parent.id\&\&node.name===name){return node}"],

	["function transitionParam(param,value,time,duration){if(isEdge",
	 "function transitionParam(param,value,time,duration){if(true"],

	["function setParam(param,value){if(isEdge",
	 "function setParam(param,value){if(true"],

	["function _glGetIntegerv(name_,p){emscriptenWebGLGet(name_,p,\"Integer\")}",
	 "function _glGetIntegerv(name_,p){if(name_===32937){HEAP32[p>>2]=0}else{emscriptenWebGLGet(name_,p,'Integer')}}"],
],
wr: [
	["var wr={requestInstances:{},nextRequestId:1};function _JS_WebRequest_Abort(request){",
	 "var wr={requestInstances:{},nextRequestId:1};Module[\"wr\"]=wr;Module[\"wr_overrider\"]=function(request,payload){return payload;};Module[\"wr_headers\"]={};Module[\"wr_urls\"]={};function _JS_WebRequest_Abort(request){"],

	["wr.requestInstances[wr.nextRequestId]=http;return wr.nextRequestId++",
	 "wr.requestInstances[wr.nextRequestId]=http;Module.wr_urls[wr.nextRequestId]=_url;return wr.nextRequestId++"],

	["delete http;wr.requestInstances[request]=null",
	 "delete http;delete wr.requestInstances[request];delete Module.wr_headers[request];delete Module.wr_urls[request];wr.requestInstances[request]=null"],

	["function _JS_WebRequest_Send(request,ptr,length){var http=wr.requestInstances[request];try{if(length>0){var postData=HEAPU8.subarray(ptr,ptr+length);http.send(postData)}else http.send()}catch(e){console.error(e.name+\": \"+e.message)}}",
	 "function _JS_WebRequest_Send(request,ptr,length){var _url=Module.wr_urls[request];if(_url===\"https://cdp.cloud.unity3d.com/v1/events\"||_url===\"https://config.uca.cloud.unity3d.com\"){_JS_WebRequest_Release(request);return}var http=wr.requestInstances[request];if(length>0){var payload0=HEAPU8.subarray(ptr,ptr+length)}else{var payload0=new Uint8Array}override_result=Module[\"wr_overrider\"](request,payload0);if(typeof override_result.then!==\"undefined\"){override_result.then((payload=>{try{http.send(payload)}catch(e){console.error(e.name+\": \"+e.message)}}))}else{try{http.send(override_result)}catch(e){console.error(e.name+\": \"+e.message)}}}"],

	["function _JS_WebRequest_SetRequestHeader(request,header,value){var _header=Pointer_stringify(header);var _value=Pointer_stringify(value);wr.requestInstances[request].setRequestHeader(_header,_value)}",
	 "function _JS_WebRequest_SetRequestHeader(request,header,value){if(!(request in Module.wr_headers)){Module.wr_headers[request]={}}var _header=Pointer_stringify(header);var _value=Pointer_stringify(value);Module.wr_headers[request][_header]=_value;wr.requestInstances[request].setRequestHeader(_header,_value)}"],

	["req.setRequestHeader(\"Range\",\"bytes=\"+offset+\"-\"+(size+offset-1));req.setRequestHeader(\"Pragma\",\"no-cache\");req.setRequestHeader(\"Cache-Control\",\"no-cache\");req.setRequestHeader(\"If-Modified-Since\",\"Thu, 01 Jun 1970 00:00:00 GMT\");",
	 ""],

	["Module[\"abort\"]=abort;",
	 "Module[\"abort\"]=abort;Module[\"nya_patched\"]=true;Module[\"Browser\"]=Browser;"],
],
raf: [
	["_emscripten_set_main_loop_timing(mode,value){",
	 "_emscripten_set_main_loop_timing(mode,value){mode=1;value=1;"],
],
nocri: [
	["function _criFsWeb_Start(handle,urlptr,offset,size,onloadCb,onerrorCb){",
	 "function _criFsWeb_Start(handle,urlptr,offset,size,onloadCb,onerrorCb){return;"],
],
}