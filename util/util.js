const logDateFormat = {dateStyle: 'short', timeStyle: 'medium', hour12: false};

function log(x) {
	let time = new Date().toLocaleString(undefined, logDateFormat);
	console.log(`[${time}] `, x);
}

function replaceMulti(x, replacements) {
	let [a, b] = replacements[0];
	let parts = x.split(a);
	if (replacements.length > 1) {
		return parts.map(part => replaceMulti(part, replacements.slice(1))).join(b);
	} else {
		return parts.join(b);
	}
}

function replaceObjs(x, d) {
	if (x === null) return null;
	var r = false;
	for (let [k, v] of Object.entries(x)) {
		if (v !== null && typeof v === 'object') {
			r |= replaceObjs(v, d);
		} else if (v in d) {
			x[k] = d[v];
			r = true;
		}
	}
	return r;
}

function startPoll(interval, jitter, callback) {
	var r = {};
	r.i = setInterval(function () {
		r.j = setTimeout(callback, jitter * interval * Math.random());
	}, interval);
	return r;
}

function stopPoll(r) {
	if (r?.i) {
		clearInterval(r.i);
	}
	if (r?.j) {
		clearTimeout(r.j);
	}
}

// From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
function escapeRegExp(string) {
	return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function parseInterval(str, min=60) {
	if (!isNaN(+str)) return Math.max((+str)*60, min);
	const units = {s: 1, m: 60, h: 3600, d: 86400};
	const regex = / *(\d+\.?\d*)([smhd]) */g;
	const matches = Array.from(str.matchAll(regex));
	const parsed = matches.reduce((s, m) => s + (+m[1])*units[m[2]], 0);
	return Math.max(parsed, min);
}

function formatBinarySize(x, n=2) {
	if (x==0) return '0';
	let k = Math.floor(Math.log2(x)/10);
	if (k==0) n = 0;
	let suffix = {0: 'B', 1: 'KiB', 2: 'MiB', 3: 'GiB'}[k];
	return (x/2**(k*10)).toFixed(n)+suffix;
}

var db;
function openDb() {
	var req = indexedDB.open('db', 1);
	req.onsuccess = function (evt) {
		db = this.result;
	};
	req.onerror = function (evt) {
		console.error("openDb:", evt.target.errorCode);
	};

	req.onupgradeneeded = function (evt) {
		var store = evt.currentTarget.result.createObjectStore('log', { keyPath: 'id', autoIncrement: true });
	};
}
openDb();

function logPersistent(data) {
	db.transaction('log', 'readwrite').objectStore('log').add(data);
}

function getLogs() {
	const prom = new Promise((resolve, reject) => {
		const req = db.transaction('log', 'readwrite').objectStore('log').getAll();
		req.onsuccess = (event) => {
			resolve(req.result)
		}
	});
	return prom
}

function download(data) {
	const blob = new Blob([JSON.stringify(data)], {type: "application/json",});
	const url = URL.createObjectURL(blob);
	browser.downloads.download({url: url, filename: 'logs.json'})
	urlCleanup = (delta) => {
		if (delta.state && delta.state.current === "complete") {
			URL.revokeObjectURL(url);
			browser.downloads.onChanged.removeListener(urlCleanup);
		}
	}
	browser.downloads.onChanged.addListener(urlCleanup);
}