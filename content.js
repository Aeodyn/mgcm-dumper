if (document.getElementById('gameContainer')) {

	// Modified from https://stackoverflow.com/a/50730532
	function injectScript(path) {
		return new Promise((resolve, reject) => {
			const script = document.createElement('script');
			script.src = browser.runtime.getURL(path);
			(document.head || document.documentElement).appendChild(script);
			script.onload = () => resolve();
		})
	}

	function relay(data) {
		window.postMessage({mgcm: true, topage: true, ...data}, "*");
	}

	async function receivePageMessage(event) {
		if (event.source == window &&
			event.data &&
			event.data.mgcm === true && 
			event.data.topage === false) {
			port.postMessage(event.data);
		}
	}
	async function receiveAddonMessage(request) {
		switch (request) {
			case "inject":
				await injectScript('util/pako.js');
				await injectScript('util/crypt.js');
				await injectScript('script.js');
				break;
			case "dump-dresses":
				await injectScript('dump/dresses.js');
				break;
			case "dump-orbs":
				await injectScript('dump/orbs.js');
				break;
			case "dump-both":
				await injectScript('dump/both.js');
				break;
			case "dump-sf":
				await injectScript('dump/sf.js');
				break;
			default:
				relay(request);
		}
	}

	var port = browser.runtime.connect();

	port.onMessage.addListener(receiveAddonMessage);

	window.addEventListener("message", receivePageMessage);
}
