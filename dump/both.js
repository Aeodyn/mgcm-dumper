if(typeof gameInstance !== 'undefined' && gameInstance) {
	fetchCards()
	.then(CardsToSheet)
	.then(csv => {if (csv) console.save(csv, "dresses.csv")})
	.catch(log);

	fetchRunes()
	.then(RunesToSheet)
	.then(csv => {if (csv) console.save(csv, "orbs.csv")})
	.catch(log);
}
