const form = document.querySelector('form');


function toggleMatching(form, patterns) {
	const group = [];
	for (const pattern of patterns) {
		const regex = new RegExp(pattern.split('*').map(escapeRegExp).join(`.*`));

		for (const element of form.elements) {
			if (element.type === 'checkbox' && regex.test(element.id)) {
				group.push(element);
			}
		}
	}

	const value = !group.every(x => x.checked);

	for (const element of group) {
		element.checked = value;
	}
	form.onchange();
}

// From https://stackoverflow.com/a/20240387
function setAtPath(obj, path, value) {
	var a = path.split('.');
	var o = obj;
	for (var i = 0; i < a.length - 1; i++) {
		var n = a[i];
		if (n in o) {
			o = o[n];
		} else {
			o[n] = {};
			o = o[n];
		}
	}
	o[a[a.length - 1]] = value;
}

// From https://stackoverflow.com/a/56253298
function flattenObj(obj, parent='', res={}) {
	for(let key in obj) {
		let propName = parent ? parent + '.' + key : key;
		if(typeof obj[key] == 'object'){
			flattenObj(obj[key], propName, res);
		} else {
			res[propName] = obj[key];
		}
	}
	return res;
}

function changeListener() {
	const formdata = new FormData(form);
	const features = {};

	for (let [key, value] of formdata) {
		setAtPath(features, key, value);
	}

	browser.storage.local.set({'features': features});
}


if (typeof browser.webRequest.filterResponseData === 'undefined') {
	for (const id of ['sfBoss', 'sfBoss.enabled', 'bellShop', 'bellShop.enabled']) {
		document.getElementById(id).disabled = true;
	}
}

for (const x of document.querySelectorAll('[togglepattern]')) {
	x.onclick = () => toggleMatching(form, x.getAttribute('togglepattern').split(' '));
}

browser.storage.local.get('features').then(x => {
	let flattened = flattenObj(x['features']);
	for (key in flattened) {
		element = document.getElementById(key);
		if (!element) continue;
		if (element.type === 'checkbox') {
			element.checked = true;
		} else if (key === 'redirect.platform') {
			document.getElementById(key+'.'+flattened[key]).checked = true;
		} else {
			element.value = flattened[key];
		}
	}
}).then(() => {
	for (const x of document.querySelectorAll('legend > input[type="checkbox"]')) {
		x.parentElement.parentElement.classList.toggle('collapsed', !x.checked);
		x.onchange = (e) => {
			e.target.parentElement.parentElement.classList.toggle('collapsed', !e.target.checked);
		}
	}
});

form.onchange = changeListener;

document.getElementById('dump-logs').onclick = () => getLogs().then(download);
document.getElementById('clear-logs').onclick = () => db.transaction('log', 'readwrite').objectStore('log').clear();
navigator.storage.estimate().then(x => document.getElementById('log.storage_message').textContent = `Usage: ${formatBinarySize(x.usage)} / ${formatBinarySize(x.quota)}`);
